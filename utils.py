import os
import re
from Bio import SeqIO
from itertools import product
import pandas as pd
import operator, math
import sklearn

def get_metabat_bin_id(filename):
    #filename format of bins from metabat
    pattern = re.compile(r'^bin\.(.*)\.fa$')
    m = pattern.search(filename)
    if m is None:
        print(filename + ' is not a valid metabat bin')
        return None
    else:
        return m.group(1)

def metabat_bin_to_df(metabat_bins_dir, outfile=''):
    """_summary_
    Given a metabat2 bin path, return a dataframe with scaffold_id, bin_id, scaffold_size, bin_size
    Returns:
        dataframe: scaffold_id, bin_id, scaffold_size, bin_size
    """ 
    contigs = []
    bins = []
    for binfile in os.listdir(metabat_bins_dir):
        bin_id = get_metabat_bin_id(binfile)
        if bin_id:
            with open(os.path.join(metabat_bins_dir, binfile), 'r') as IN:
                total_length = 0
                for record in SeqIO.parse(IN, format='fasta'):
                    total_length += len(str(record.seq))
                    contigs.append({
                        'Contig': record.id, 
                        'length': len(record.seq),
                        'bin_id': bin_id,
                        })
                bins.append({'bin_id':bin_id, 'bin_size': total_length})
    contigs = pd.DataFrame(contigs)            
    bins = pd.DataFrame(bins)
    bins = pd.merge(contigs, bins, on='bin_id', how='left')
    if outfile == '':
        return bins
    else:
        bins.to_pickle(outfile)

def get_bin_stats(bins_dir, stat_file):
    if not os.path.exists(stat_file):
        metabat_bin_to_df(bins_dir, stat_file)
    return pd.read_pickle(stat_file)

# faster cosine similarity according to https://gist.github.com/mckelvin/5bfad28ceb3a484dfd2a
def dot_product2(v1, v2):
    return sum(map(operator.mul, v1, v2))

def cosinesimilarity2(v1, v2):
    prod = dot_product2(v1, v2)
    len1 = math.sqrt(dot_product2(v1, v1))
    len2 = math.sqrt(dot_product2(v2, v2))
    return prod / (len1 * len2)

reverse_complement =  ''.maketrans({'A':'T', 'T':'A', 'G':'C', 'C':'G'})
def tnf_dict():
    # return tnf dictionary, key is 4-mer, value is vector index
    alphabets = [['A', 'C', 'G', 'T'], ['A', 'C', 'G', 'T'], ['A', 'C', 'G', 'T'], ['A', 'C', 'G', 'T']]
    # all possible 4-mers (256)
    result = [''.join(p) for p in list(product(*alphabets))]
    # cononical 4-mers (136)
    tetra = {}
    counter = 0
    for r in result:
        rc = r.translate(reverse_complement)[::-1]
        if r <= rc:
            tetra[r] = counter
            counter +=1           
    return tetra

tetra = tnf_dict()
def tnf(seq, tetra=tetra):
    # use last element for non-alphabets
    tnf_vector = [0]*137
    seq = seq.upper()
    # initialize the queues
    num_kmers = len(seq)-3
    for i in range(0, num_kmers):
      tnf_vector[tetra.get(min(seq[i:i+4], seq[i:i+4].translate(reverse_complement)[::-1]), 136)] +=1
    num_kmers = sum(tnf_vector[0:136]) # not counting non-alphabets
    return [t/num_kmers for t in tnf_vector[0:136]]

def reads_tnf(reads, method='median', sample=10):
  """
  Given a vector a reads, return tnf of a subset of the reads
  take the mean or median of tnf as the cluster tnf
  """
  # sort the reads by length, only use long ones
  if sample>0:
    reads = sorted(reads, key=len, reverse=True)[0:sample]
  reads_tnf = [tnf(r) for r in reads]
  reads_tnf = pd.DataFrame(reads_tnf)
  if method == 'median':
    return list(reads_tnf.median(axis=0))
  else:
    return list(reads_tnf.mean(axis=0)) 

def cal_distance(v1, v2, metric='cosine'):
    """_calcuate distance of two vectors_

    Args:
        v1 (_list_): _Vector 1_
        v2 (_list_): _Vector 2_
        metric (str, optional): _distance metric (cosine or euclidean_. Defaults to 'cosine'.
    """
    if metric == 'euclidean':
        return sklearn.metrics.pairwise.euclidean_distances(v1, v2)
    else:
        return cosinesimilarity2(v1, v2)
