#!/usr/bin/env python
"""_check quality given groundtruth_
Outputs purity of each bin and completeness of each genomes
Data not in the groundtruth are ignored.
"""
import pandas as pd
import numpy as np
import sys
from utils import metabat_bin_to_df

def contig_in_bin(contig, mbin, bin_purity, gt):
    genomes = bin_purity.loc[str(mbin), 'composition'][0]
    if contig in gt.index:
        if gt.loc[contig, 'genome'] in genomes:
            return 1
        else:
            return -1
    return 0

def cal_purity(gt, bins):
    gt = (pd
        .merge(gt, bins.set_index('Contig'), left_index=True, right_index=True, how='left')
        .fillna({'bin_id':'-1'})
        )
    purity = gt.groupby(['bin_id', 'genome']).agg({'length': sum})
    # purity of each bin
    bin_purity = []
    for mbin in set(purity.index.get_level_values(0)):
        mbin_to_g = purity.xs(mbin, level=0)
        genome = mbin_to_g.idxmax()
        p_genome = mbin_to_g.max()/mbin_to_g.sum()
        genomes = []
        for g in mbin_to_g.index:
            genomes.append({g:float(mbin_to_g.loc[g, 'length']/mbin_to_g.sum())})
        bin_purity.append([mbin, genome[0], p_genome[0], genomes])
    bin_purity = pd.DataFrame(bin_purity, columns=['bin_id', 'genome', 'bin_purity', 'composition'])
    bin_purity = bin_purity.reset_index().drop('index', axis=1).set_index('bin_id')
    return bin_purity[bin_purity.index != '-1'].sort_values(by='bin_purity', ascending=True)

def cal_completeness(gt, bins):

    gt = (pd
        .merge(gt, bins.set_index('Contig'), left_index=True, right_index=True, how='left')
        .fillna({'bin_id':'-1'})
        )
    completeness = gt.groupby(['genome','bin_id', ]).agg({'length': sum})
    # completeness of each bin
    bin_completeness = []
    for g in set(completeness.index.get_level_values(0)):
        bins_in_g = completeness.xs(g, level=0)
        largest_bin = bins_in_g.idxmax()
        c_bin = bins_in_g.max()/bins_in_g.sum()
        bins = []
        for b in bins_in_g.index:
            bins.append({b:float(bins_in_g.loc[b, 'length']/bins_in_g.sum())})
        bin_completeness.append([g, largest_bin[0], c_bin[0], bins])
    bin_completeness = pd.DataFrame(bin_completeness, columns=['genome', 'bin', 'genome_completeness', 'composition'])
    return bin_completeness.sort_values(by='genome_completeness', ascending=False)

def main(ground_truth_file, bins_dir, prefix=''):
    
    gt = pd.read_csv(ground_truth_file, sep='\t', index_col=0)
    bins = metabat_bin_to_df(bins_dir)[['Contig', 'bin_id']]
    cal_purity(gt, bins).to_csv(prefix + '_purity.tsv', sep='\t')
    cal_completeness(gt, bins).to_csv(prefix + '_completeness.tsv', sep='\t')

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3])

