"""
Make feature matrix, train a random forest model based on selected bins

""" 
import numpy as np
import pandas as pd
import sys, os
import matplotlib.pyplot as plt
import joblib
from statistics import mean
from sklearn.utils import resample
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import export_graphviz
from utils import *

from config import depth_file, metabat_bins_dir, outdir, top, down_sampling
hic_mapped_read_tsv = sys.argv[1]
        
def scaffolds_pcombos(bin_list, metabat_bins_stat):
    """_summary_
    generate pairwise scaffold combos from a list of bin ids
    paris from the same-bin get score 1 otherwise 0
    Returns:
        _DataFrame_: _pairwise combo_
    """ 
    bin_list = pd.DataFrame(bin_list, columns=['bin_id'])
    selected = (pd
                .merge(metabat_bins_stat, bin_list, on='bin_id', how='inner')
                .groupby('bin_id')
                .agg({'Contig': list})
                .explode('Contig')
    ).reset_index()
    selected['key'] = 1
    # self cross join to create all pair combos
    metabat_df = pd.merge(
        selected.rename(columns={'Contig':'pair1'}),
        selected.rename(columns={'Contig':'pair2'}), 
        on='key', suffixes=['1', '2']).drop('key', axis=1)
    # remove B-A pair and A-A pair, only retain A-B pair
    metabat_df = metabat_df[metabat_df['pair1'] < metabat_df['pair2']]
    # set same Bin_Id to 1 otherwise 0
    metabat_df['metabat_bin_score'] = (metabat_df['bin_id1'] == metabat_df['bin_id2']).astype(int)
    metabat_df = metabat_df[['pair1', 'pair2', 'metabat_bin_score']]
    return metabat_df

def select_bins(metabat_bins_stat, top=10):
    """_select bins for training_

    Returns:
        _Dataframe_: _list of bins_
    """
    print("select largest bins as training.")
    selected_list = (metabat_bins_stat
                        .groupby('bin_id')
                        .agg({'length':sum})
                        .sort_values(by='length', ascending=False)
                        .index[0:top]
                        ).tolist()
        
    print("bins to be used for training: ")
    print(' '.join(selected_list))   
    
    return selected_list

def import_hic_mapping(outdir, hic_mapped_read_tsv, plotting=True):
    """_Parse long range data, filter_

    Returns:
        _None_: _None_
    """
    # use batch load to save memory
    iter_csv = pd.read_csv(hic_mapped_read_tsv, 
                           sep='\t', 
                           comment='@', 
                           header=None,
                           usecols=[0, 1, 2],
                           names=['read', 'pair1', 'pair2'],
                           iterator=True, 
                           chunksize=1000000
                           )
    # concatenate according to a filter to our result dataframe
    mapped = pd.concat(
        [chunk[(chunk['pair1'] != '=') 
               & (chunk['pair2'] != '=')
               & (chunk['pair1'] != chunk['pair2'])
               ] for chunk in iter_csv]
        )  
 
    mapped['pair'] = mapped.loc[:,['pair1','pair2']].apply(lambda x: ','.join(sorted(x)),axis=1)
    mapped.drop(columns=['pair1','pair2'], inplace=True, axis=1)

    counts = mapped.groupby('pair').count()

    read_distribution_figure = (outdir+'/reads_mapped_to_different_contigs.pdf')
    #Read Distribution

    if plotting:
        ax = plt.subplot(111)
        ax.set_title('Read Distribution')
        ax.set_xlabel('Number of Scaffold-Scaffold Pairs')
        ax.set_ylabel('Number of Hi-C Reads Connecting Two Scaffolds')
        counts.plot(kind="hist", 
                    bins=1000, 
                    logx=True, 
                    title='Read Distribution',
                    xlabel='Number of Scaffold-Scaffold Pairs',
                    ylabel='Number of Hi-C Reads Connecting Two Scaffolds',
                    ax=ax
                    #giving errors
                   )

        plt.savefig(read_distribution_figure)
        plt.clf()
    #Read Percentile Cutoff
    filtered = counts.reset_index()
    # save this for later
    filtered[['pair1', 'pair2']] = filtered.pair.str.split(',', expand=True)
    filtered.drop('pair', axis=1).to_pickle(outdir+'/reads_mapped_to_diff_contigs.pkl')
    return 

def make_feature_matrix(outdir, depth_file):
    """_make feature matrix_

    Returns:
        _None_: _None_
    """
    depth_df = pd.read_csv(
        depth_file,
        sep='\t',
        skiprows=1,
        header=None,
        usecols=[0,1,2,4],
        names=['contig', 'length', 'avg_depth', 'var_depth'],
        dtype={'contig':str, 'length':int, 'avg_depth':float, 'var_depth':float}
    )
    depth_df['contig']=depth_df['contig'].str.split(' ').str[0]
    reads_mapped_to_diff_contigs_file = outdir+'/reads_mapped_to_diff_contigs.pkl'
    filtered = (pd
        .read_pickle(reads_mapped_to_diff_contigs_file)
        .reset_index() 
    )
    # Merge Pair 1 with Depth DF
    filtered = pd.merge(
        filtered, 
        depth_df[['contig', 'length', 'avg_depth', 'var_depth']].rename(columns=
            {
                'length': 'len1',
                'avg_depth': 'cov1',
                'var_depth': 'var1'
            }
        ), 
        left_on='pair1', 
        right_on='contig',
        how='left'
    ).drop('contig', axis=1)

    # Merge Pair 2 with Depth DF 
    filtered = pd.merge(
        filtered, 
        depth_df[['contig', 'length', 'avg_depth', 'var_depth']].rename(columns=
            {
                'length': 'len2',
                'avg_depth': 'cov2',
                'var_depth': 'var2'
            }
        ), 
        left_on='pair2', 
        right_on='contig',
        how='left'
    ).drop('contig', axis=1)
    # coverage information
    filtered['cov_d'] = abs(np.log2(filtered['cov1']/filtered['cov2']))
    filtered['cov_p'] = np.log10(filtered['cov1']*filtered['cov2'])   
    filtered['var']= np.log10(filtered['var1']*filtered['var2'])
    
    # link intensity number of links each pair has
    p1 = filtered.groupby('pair1').count()['read']
    p2 = filtered.groupby('pair2').count()['read']
    filtered = pd.merge(filtered, p1.rename('links1'), left_on='pair1', right_index=True, how='left' )
    filtered = pd.merge(filtered, p2.rename('links2'), left_on='pair2', right_index=True, how='left' )
    filtered = filtered.fillna({'links1': 0, 'links2':0})
    filtered['link_intensity']= np.log10(filtered['links1']*filtered['links2'])
    
    # normalize hi-c counts
    filtered['norm'] = 1.0e+6 * filtered['read']/(filtered['len1']*filtered['len2']**0.5)
    contig_pair_to_ml_df=filtered[['pair1', 'pair2', 'norm', 'cov_d', 'cov_p','var', 'link_intensity', 'read']].fillna(0)
    contig_pair_to_ml_df.to_pickle(outdir+'/feature_matrix_all.pkl')
    return
  

def make_ml_matrix(
        outdir,
        metabat_bins_dir, 
        top=10,
        down_sampling=False,
    ):
    """_summary_
    make dataframe for random forest training
    Returns:
        dataframe: a pandas dataframe contains training data
    """     
    # generate label from these bins
    metabat_bins_stat = get_bin_stats(metabat_bins_dir, outdir+'/original_bins_stat.pkl')
    bins_to_train = select_bins(metabat_bins_stat, 
                                top=top,                                
                                )
    scaffold_pairs_combo = scaffolds_pcombos(bins_to_train, metabat_bins_stat)
    ml_matrix = (pd
        .read_pickle(outdir+'/feature_matrix_all.pkl')
        .reset_index() 
    )
    ml_matrix2 = pd.merge(ml_matrix, scaffold_pairs_combo, on=['pair1', 'pair2'], how='inner')

    if down_sampling:
        #Balancing ML Dataframe through Downsampling
        #Defining Majority and Minority Sample
        df_majority=ml_matrix2[ml_matrix2['metabat_bin_score']==0]
        df_minority=ml_matrix2[ml_matrix2['metabat_bin_score']==1]

        print('DF 0 Shape before downsampling: ', df_majority.shape)
        print('DF 1 Shape before downsampling: ', df_minority.shape)
        
        if df_minority.shape[0] < 50:
            print("Too few samples to make the training set, stop.")
            sys.exit(0)
            
        if(df_majority.shape[0] < df_minority.shape[0]):
            df_majority=ml_matrix2[ml_matrix2['metabat_bin_score']==1]
            df_minority=ml_matrix2[ml_matrix2['metabat_bin_score']==0]

        df_majority_downsampled = resample(df_majority, replace=False, n_samples=df_minority.shape[0], random_state=123)

        ml_matrix2=pd.concat([df_majority_downsampled,df_minority])

    return ml_matrix2

def train_RF(outdir, data_df, plot_importance=True, plot_roc=True):
    """_summary_
    train a random forest model
    Returns:
        dataframe: a pandas dataframe contains training data
    """ 
    #split dataset in features and target variable
    feature_cols = ['norm', 'cov_d', 'cov_p','var', 'link_intensity', 'read']
    X = data_df[feature_cols] # Features
    y = data_df.metabat_bin_score # Target variable
    X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2,random_state=1234)
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1234)

    # training a RandomForest
    rf_model = RandomForestClassifier(
        random_state=1234,
        class_weight={0:1,1:100}, 
        n_estimators=100, 
        n_jobs=-1).fit(X_train, y_train) 
    rf_predictions = rf_model.predict(X_val) 

    # creating a confusion matrix 
    metrics.confusion_matrix(y_val, rf_predictions) 

    print("Validation Accuracy: %.3f." % metrics.accuracy_score(y_val, rf_predictions))
    print("Validation Precision: %.3f." % metrics.precision_score(y_val, rf_predictions))
    print("Validation Recall: %.3f." % metrics.recall_score(y_val, rf_predictions))

    estimator = rf_model.estimators_[5]

    # Export as dot file
    export_graphviz(estimator, out_file=outdir+'/tree.dot', feature_names = feature_cols, rounded = True, proportion = False, precision = 2, filled = True)

    if  plot_importance:
        # get importance
        #Gini or mean decrease impurity importance
        importance = rf_model.feature_importances_
        # plot feature importance
        #pyplot.bar([x for x in range(len(importance))], importance)
        #pyplot.clf()
        plt.barh(feature_cols, rf_model.feature_importances_)
        barplot_file=outdir+"/gini_mean_decrease_impurity_importance.png"
        plt.savefig(barplot_file)
        plt.clf()

    #joblib file 

    joblib_filename = outdir+"/balanced_random_forest_model.sav"
    joblib.dump(rf_model, joblib_filename)

    #checking joblib
    loaded_joblib_model = joblib.load(joblib_filename)
    result = loaded_joblib_model.score(X_test, y_test)
    test_predictions = loaded_joblib_model.predict(X_test)
    print(result)

    print("Testing Accuracy: %.3f." % metrics.accuracy_score(y_test, test_predictions))
    print("Testing Precision: %.3f." % metrics.precision_score(y_test, test_predictions))
    print("Testing Recall: %.3f." % metrics.recall_score(y_test, test_predictions))

    if plot_roc:
        r_probs = [0 for _ in range(len(y_test))]
        rf_probs = loaded_joblib_model.predict_proba(X_test)
        rf_probs = rf_probs[:, 1]

        rf_auc = metrics.roc_auc_score(y_test, rf_probs)
        r_auc = metrics.roc_auc_score(y_test, r_probs)
        r_fpr, r_tpr, _ = metrics.roc_curve(y_test, r_probs)
        rf_fpr, rf_tpr, _ = metrics.roc_curve(y_test, rf_probs)

        plt.plot(r_fpr, r_tpr, linestyle='--', label='Random prediction (AUROC = %0.3f)' % r_auc)
        plt.plot(rf_fpr, rf_tpr, marker='.', label='Random Forest (AUROC = %0.3f)' % rf_auc)

        plt.title('ROC Plot')
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.legend() # 
        roc_file=outdir + "/roc_plot.png"
        plt.savefig(roc_file)
        plt.clf()
        
# main #       
if os.path.exists(outdir+'/reads_mapped_to_diff_contigs.pkl'):
    print("Reads mapping has been previously processed, skipping.")
else:  
    import_hic_mapping(outdir, hic_mapped_read_tsv, plotting=True) 
if os.path.exists(outdir+'/feature_matrix_all.pkl' ):
    print("Feature matrix has been previously made, skipping.")
else:
    make_feature_matrix(outdir, depth_file)       
data_df = make_ml_matrix(    
    outdir,
    metabat_bins_dir, 
    top=top,
    down_sampling=down_sampling,
)
train_RF(outdir, data_df)
