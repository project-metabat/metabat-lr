"""_bin statistics_
"""
import sys, os
import pandas as pd
from utils import metabat_bin_to_df

def main(bins_dir, stat_file):
    if not os.path.exists(stat_file):
        metabat_bin_to_df(bins_dir, stat_file)
    stats = pd.read_pickle(stat_file) 
    # bin size, contig size, contigs per bin distribution
    output = pd.concat([
     stats.drop_duplicates('bin_id')[['bin_size']].describe(),
     stats['length'].describe(),
     stats.groupby('bin_id').size().rename('contigs/bin').describe()   
    ], axis=1)
    print("Bin statistics:\n==============================================")
    print(output.apply(lambda s: s.apply(lambda x: format(x, 'g'))))
    print("==============================================")   

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
