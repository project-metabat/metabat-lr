import pandas as pd
import shutil
import networkx as nx
from config import outdir, scaffolds, depth_file, metabat_bins_dir, min_score, min_tnf_score, min_depth_score, min_LR_score
from Bio import SeqIO
import joblib
from scipy.stats import ttest_ind
from utils import *

def apply_ML(model_file, feature_matrix_file):
    """_Apply ML model to new data_

    Args:
        model_file (_type_): _ML model_
        feature_matrix_file (_type_): _['norm', 'cov_d', 'cov_p','var', 'link_intensity', 'read']_

    Returns:
        _DataFrame_: _'pair1', 'pair2', 'read', 'prob'_
    """
    feature_matrix = (pd
        .read_pickle(feature_matrix_file)
    ).fillna(0)
    #Load Random Forest Model
    rf_model = joblib.load(model_file)
    feature_cols = ['norm', 'cov_d', 'cov_p','var', 'link_intensity', 'read']
    model_predictions = rf_model.predict_proba(feature_matrix[feature_cols])

    feature_matrix['prob'] = pd.DataFrame(model_predictions).rename(columns={1:'prob'})['prob']
    return feature_matrix[['pair1', 'pair2', 'read', 'prob']]
    
def ncomb(x, metabat_contigs_per_bin):
    return  metabat_contigs_per_bin.loc[x[0]] * metabat_contigs_per_bin.loc[x[1]]
    
def cal_bin_LR_score(predictions, metabat_contig_bin, min_score=0.50):
    """"Generate similarity score between bins based on long range info """
    
    predictions = predictions[predictions['prob']>= min_score]

    # Combining Metabat Bin Information with Contig-Contig Matrix 
    #Add Metabat Bin Info For Pair1 
    contig_pairs = (pd
        .merge(predictions[['pair1', 'pair2', 'read','prob']], metabat_contig_bin,left_on='pair1', right_on='Contig', how='left')
        .drop('Contig', axis=1)
        .fillna({'bin_id': '0'}) 
        .rename(columns={'bin_id':'metabat_bin1'}) 
    )
    #Add Metabat Bin Info For Pair2 
    contig_pairs = (pd
        .merge(contig_pairs,metabat_contig_bin,left_on='pair2',right_on='Contig',how='left')
        .drop('Contig', axis=1)
        .fillna({'bin_id': '0'}) 
        .rename(columns={'bin_id':'metabat_bin2'})
    )
    metabat_contigs_per_bin = (metabat_contig_bin
        .groupby(['bin_id'])
        .size()
    )
    sel = (contig_pairs['metabat_bin1'] != '0') &  (contig_pairs['metabat_bin2'] != '0') & (contig_pairs['metabat_bin1'] != contig_pairs['metabat_bin2'])
    contig_pairs['bpair'] = (contig_pairs.loc[sel, ['metabat_bin1', 'metabat_bin2']]
                             .apply(lambda x: ':'.join([str(a) for a in sorted(x)]), axis=1)
                            )
    bin_pairs = contig_pairs.groupby('bpair').size().reset_index().rename(columns={0:'read'})
    # bin_pairs = contig_pairs.groupby('bpair').agg({'recruit_score':sum}).reset_index().rename(columns={'recruit_score':'read'})
    bin_pairs['ncomb'] = (bin_pairs[['bpair']]
                          .apply(lambda x: ncomb(x.str.split(':')[0], metabat_contigs_per_bin), axis=1)
                         )
    bin_pairs['LR_score'] = bin_pairs['read']/bin_pairs['ncomb']
    return bin_pairs

def cal_bin_tnf(metabat_contig_bin, scaffolds_seq, sample=3):
    """_calculate tnf vectors of all bins, take the median of the three largest contigs_"""

    tnf_dict = {}
    for b in metabat_contig_bin['bin_id'].tolist():
        contigs = metabat_contig_bin.loc[metabat_contig_bin['bin_id'] == b, 'Contig'].tolist()
        seqs = [str(scaffolds_seq[r].seq) for r in contigs ]
        tnf_dict[b] = reads_tnf(seqs, sample=sample)
    return tnf_dict

def cal_bin_depth(metabat_contig_bin, depth_df, sample=10):
    """"calcuate a vector of depths of all contigs in a bin"""
    
    depth_dict = {}
    for b in metabat_contig_bin['bin_id'].tolist(): 
        if sample > 0:          
            contigs = (metabat_contig_bin
                       .sort_values(by='length', ascending=False)
                       .loc[metabat_contig_bin['bin_id'] == b, 'Contig']
                       ).tolist()[0:sample]
        else:
            contigs = metabat_contig_bin.loc[metabat_contig_bin['bin_id'] == b, 'Contig'].tolist()
        depth_dict[b] = [float(depth_df.loc[depth_df['contig']==r, 'avg_depth']) for r in contigs]                          
    return depth_dict

def merging(
        contig_pairs,
        metabat_contig_bin,
        depth_df,
        min_score=0.5,
        min_tnf_score=0.995,
        min_depth_score=0.7,
        min_LR_score=0.7,
    ):
    bin_pairs = cal_bin_LR_score(contig_pairs, metabat_contig_bin, min_score=min_score)
    tnf_dict = cal_bin_tnf(metabat_contig_bin, scaffolds_seq)
    depth_dict = cal_bin_depth(metabat_contig_bin, depth_df)
    # depth_score is the p_value of t-test
    bin_pairs['depth_score'] = (bin_pairs
                                .bpair.str.split(':', expand=True)
                                .apply(lambda x: ttest_ind(depth_dict[x[0]], depth_dict[x[1]]).pvalue, axis=1)
    )
    # tnf_score is the consine similariry of the two tnf vecdtors
    bin_pairs['tnf_score'] = (bin_pairs
                              .bpair.str.split(':', expand=True)
                              .apply(lambda x: cal_distance(tnf_dict[x[0]], tnf_dict[x[1]]), axis=1)
    )
    bin_pairs.to_csv(outdir + '/bin_pairs.tsv', sep='\t')
    to_merge = (bin_pairs['depth_score'] >= min_depth_score) & (bin_pairs['LR_score'] >= min_LR_score) & (bin_pairs['tnf_score'] >= min_tnf_score)
    bin_pairs = bin_pairs[to_merge]
    if bin_pairs.shape[0] == 0:
        print("no bins can be merged under the specified conditions.")
        return []
    merge_graph = nx.from_pandas_edgelist(
        df=(bin_pairs
                .bpair.str.split(':', expand=True)
                .rename(columns={0:'metabat_bin1', 1:'metabat_bin2'})
           ),
        source='metabat_bin1', 
        target='metabat_bin2',
    )
    return list(nx.connected_components(merge_graph))

def recruiting(
    predictions, 
    metabat_contig_bin, 
    weight='prob', 
    min_prob=0.5,
    min_reads=2,
    algorithm='lpa', # lpa, louvain
    verbose=False,
    ):
    sel = (predictions['prob'] >= min_prob) & (predictions['read'] >= min_reads)
    predictions = predictions.loc[sel, ['pair1', 'pair2', 'read', 'prob']]
    G=nx.from_pandas_edgelist(predictions, source='pair1', target='pair2', edge_attr=['read', 'prob'])
    if algorithm == 'lpa':
        communities = nx.algorithms.community.asyn_lpa_communities(G, weight=weight, seed=1234)
    elif algorithm == 'louvain':
        communities = nx.algorithms.community.louvain_communities(G, weight=weight, seed=1234)
    else:
        print("unknown algorithm.")
        return None
    labels = {}
    n = 0
    for c in communities:
        for node in c:
            labels[node] = n
        n+=1
    labels = pd.DataFrame.from_dict(labels, orient='index', columns=['label'])
    labels = (pd
              .merge(labels.reset_index(), metabat_contig_bin, left_on='index', right_on='Contig', how='left')
              .drop('Contig', axis=1)
              .fillna({'bin_id':'0'})
             )
    clusters = labels.groupby(['label', 'bin_id']).agg({'index':set})
    clusters['count'] = clusters['index'].apply(len)
    clusters = clusters.sort_values('count', ascending=False)
    new_labels = {}
    for c in set(clusters.index.get_level_values(0)):
        this_cluster = clusters.xs(c, level=0)
        if this_cluster.shape[0] > 2:
            if verbose: 
                print("cluster %d contains two or more known bins, undeciding." %c)
                print(list(this_cluster.index))   
        elif this_cluster.shape[0] == 1:
            if verbose: print("cluster %d contains only zero known bin or one known bin without unbinned scaffolds, not useful." %c) 
        else: # ==2
            if this_cluster.index[0] == '0':
                if verbose: print("cluster %d: assign labels according to the minority." %c) 
                for contig in this_cluster.loc[this_cluster.index[0], 'index']:
                    new_labels[contig] = this_cluster.index[1] 
            elif this_cluster.index[1] == '0': 
                if verbose: print("cluster %d: assign labels according to the majority." %c) 
                for contig in this_cluster.loc[this_cluster.index[1], 'index']:
                    new_labels[contig] = this_cluster.index[0] 
            else: # two known bins, not useful
                pass
    
    recruited = (pd
                 .DataFrame
                 .from_dict(new_labels, orient='index',columns=['bin_id'])
                 .reset_index()
                 .rename(columns={'index':'Contig'})
                )
    recruited.to_csv(outdir + 'recuiting.tsv', sep='\t')
    return recruited

feature_matrix_file = outdir+'/feature_matrix_all.pkl'
joblib_model_filename = outdir + "/balanced_random_forest_model.sav"

metabat_contig_bin = get_bin_stats(metabat_bins_dir, outdir + '/original_bins_stat.pkl')
predictions = apply_ML(joblib_model_filename, feature_matrix_file)

scaffolds_seq = {}
with open(scaffolds, 'rt') as SCAFFOLDS:
    for record in SeqIO.parse(SCAFFOLDS, format='fasta'):
        scaffolds_seq[record.id] = record
        
depth_df = pd.read_csv(
        depth_file,
        sep='\t',
        skiprows=1,
        header=None,
        usecols=[0,1,2,4],
        names=['contig', 'length', 'avg_depth', 'var_depth'],
        dtype={'contig':str, 'length':int, 'avg_depth':float, 'var_depth':float}
    )
depth_df['contig']=depth_df['contig'].str.split(' ').str[0]

merging_list = merging(
        predictions,
        metabat_contig_bin,
        depth_df,
        min_score=min_score,
        min_tnf_score=min_tnf_score,
        min_depth_score=min_depth_score,
        min_LR_score=min_LR_score,
    )
print("These bins are merged:")
print(merging_list)
# update bin labels
merging_dict = {}
for c in merging_list:
    new_label = '.'.join(list(c))
    for b in c:
        merging_dict[b] = new_label
metabat_contig_bin[['bin_id']] = metabat_contig_bin[['bin_id']].replace(merging_dict)

recruited = recruiting(predictions, metabat_contig_bin)

# bins affected by recruiting:
affected_bins = len(set(recruited['bin_id']))
print("%d Contigs Recruited to %d bins." % (recruited.shape[0], affected_bins))

metabat_contig_bin = pd.concat([metabat_contig_bin, recruited], axis=0)
shutil.os.mkdir(outdir + '/LR_bins')
new_bins = metabat_contig_bin.groupby('bin_id').agg({'Contig':list})
for i in new_bins.index:
    contigs = new_bins.loc[i, 'Contig']
    contigs = [scaffolds_seq[c] for c in contigs]
    filename = 'bin.' + i + '.fa'
    SeqIO.write(contigs, outdir + '/LR_bins/'+filename, format='fasta')