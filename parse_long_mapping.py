#!/usr/bin/env python

# parse long read mapping results, generate links

import sys
from itertools import combinations
filtered = {}
for line in sys.stdin:
    line=line.strip()
    if line[0] == '@':
        print(line)
    else:
        read, target = line.split('\t')
        if read in filtered:
            filtered[read].append(target)
        else:
            filtered[read] = [target]
sys.stderr.write("total mapped reads passed filter: %d. \n" % len(filtered))
split_reads = 0
for read in filtered.keys():
    filtered[read] = list(set(filtered[read]))
    if len(filtered[read])>1:
        split_reads +=1
        [print('\t'.join([read, t1, t2])) for t1,t2 in combinations(filtered[read],2) ]   
    else:
        print('\t'.join([read, filtered[read][0], '=']))   

sys.stderr.write("total split reads passed filter: %d. \n" % split_reads)       